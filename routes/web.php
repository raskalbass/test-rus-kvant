<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FeedbackController;

Route::get('/', function () {
    return view('feedback');
});

Route::post('/feedback', [FeedbackController::class, 'store']);
