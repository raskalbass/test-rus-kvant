<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Test rus-kvant</title>
</head>

<body>
    <div id="app">
        <feedback-form></feedback-form>
    </div>

    @vite('resources/js/app.js')
</body>

</html>