<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Domain\Feedback\Services\FeedbackService;

class FeedbackController extends Controller
{
    public function store(Request $request)
    {
        $storageType = config('feedback.storage');

        try {
            $service = new FeedbackService($storageType);
            $service->submitFeedback($request->all());

            return response()->json(['message' => 'Feedback successfully saved.'], 200);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }
    }
}
