<?php

namespace App\Domain\Feedback\Repositories;

use App\Domain\Feedback\Entities\Feedback;

interface FeedbackRepositoryInterface
{
    public function save(Feedback $feedback): void;
}