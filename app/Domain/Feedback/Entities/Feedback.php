<?php

namespace App\Domain\Feedback\Entities;

class Feedback
{
    public function __construct(
        private string $name,
        private string $phone,
        private string $message
    ) {
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function getMessage(): string
    {
        return $this->message;
    }
}
