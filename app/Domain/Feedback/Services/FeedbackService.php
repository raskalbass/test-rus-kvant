<?php

namespace App\Domain\Feedback\Services;

use App\Domain\Feedback\Entities\Feedback;
use App\Domain\Feedback\Repositories\FeedbackRepositoryInterface;
use App\Infrastructure\Validation\FeedbackValidator;
use App\Infrastructure\Factories\FeedbackStorageFactory;

class FeedbackService
{
    private FeedbackRepositoryInterface $feedbackRepository;

    public function __construct(string $storageType)
    {
        $this->feedbackRepository = FeedbackStorageFactory::create($storageType);
    }

    public function submitFeedback(array $data): void
    {
        $validator = new FeedbackValidator();
        $validator->validate($data);
        $feedback = new Feedback($data['name'], $data['phone'], $data['message']);
        $this->feedbackRepository->save($feedback);
    }
}
