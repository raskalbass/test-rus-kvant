<?php

namespace App\Infrastructure\Factories;

use App\Domain\Feedback\Repositories\FeedbackRepositoryInterface;
use App\Infrastructure\Repositories\EloquentFeedbackRepository;
use App\Infrastructure\Repositories\FileFeedbackRepository;

class FeedbackStorageFactory
{
    public static function create(string $type): FeedbackRepositoryInterface
    {
        return match ($type) {
            'database' => new EloquentFeedbackRepository(),
            'file' => new FileFeedbackRepository(),
            default => throw new \InvalidArgumentException("Unsupported feedback storage type: {$type}"),
        };
    }
}
