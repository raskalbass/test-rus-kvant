<?php

namespace App\Infrastructure\EloquentModels;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    protected $table = 'feedbacks';
    
    protected $fillable = ['name', 'phone', 'message'];
}
