<?php

namespace App\Infrastructure\Repositories;

use App\Domain\Feedback\Entities\Feedback;
use App\Domain\Feedback\Repositories\FeedbackRepositoryInterface;
use App\Infrastructure\EloquentModels\Feedback as EloquentFeedbackModel;

class EloquentFeedbackRepository implements FeedbackRepositoryInterface
{
    public function save(Feedback $feedback): void
    {
        $eloquentFeedback = new EloquentFeedbackModel([
            'name' => $feedback->getName(),
            'phone' => $feedback->getPhone(),
            'message' => $feedback->getMessage(),
        ]);

        $eloquentFeedback->save();
    }
}
