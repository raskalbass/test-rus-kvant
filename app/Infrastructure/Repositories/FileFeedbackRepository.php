<?php

namespace App\Infrastructure\Repositories;

use App\Domain\Feedback\Entities\Feedback;
use App\Domain\Feedback\Repositories\FeedbackRepositoryInterface;
use Illuminate\Support\Facades\Storage;

class FileFeedbackRepository implements FeedbackRepositoryInterface
{
    public function save(Feedback $feedback): void
    {
        $data = sprintf("%s, %s, %s\n", $feedback->getName(), $feedback->getPhone(), $feedback->getMessage());

        Storage::disk('local')->append('feedback.txt', $data);
    }
}
