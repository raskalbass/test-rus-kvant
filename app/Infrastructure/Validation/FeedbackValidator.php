<?php

namespace App\Infrastructure\Validation;

use Illuminate\Support\Facades\Validator;
use InvalidArgumentException;

class FeedbackValidator
{
    public function validate(array $data): void
    {
        $validator = Validator::make($data, [
            'name' => 'required|string|max:255',
            'phone' => 'required|string|max:255',
            'message' => 'required|string',
        ]);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }
    }
}
